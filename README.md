# Harman Spark HW
### CS 679 Clusters


- The big caveat to this was that I was not actually able to use `submit-spark myFile.py` to run my scripts. I am having to copy and and paste the commands into the `psypark` CLI to run the scripts. Which do run fine when I do this. 
- I also had a lot of issues when trying to open the `pyspark` interface, at times I would simply get `20/02/03 23:04:21 WARN Utils: Service 'SparkUI' could not bind on port 4041. Attempting port 4042.` errors, increasing port without any luck connecting.
- The command I uses to connect to pyspark;

```
module load hadoop
pyspark --master=yarn --num-executors 25 --driver-memory 8g --executor-memory 8
```

- Additionally debugging via the CLI was a total nightmare, the endless java logs were almost impossible to wade through to find sources of error. Debugging pretty much just included scouring through my CLI calls over and over again.

## Part 1

**dateCounts.py**: This is the main script to run spark commands for part 1.

For this part I did not quite have a full grasp of some of the pyspark functions such as `countByValue` and `reduceByKey` which typically seemed to be the much better option I learned and implemented in the later parts of the assignment. So for this problem I am simply counting all of the values of the first item in my RDD in my initial pass, but then I went back and rewrote the logic using `reduceByKey`. For writing the results I had a ton of issues trying to use the spark.write or save commands and eventually just wrote to a textfile using fopen and python. The only checking that I am doing is to make sure that there are 7 elements in each row. There are probably other checks that could have been taken, but I did not run into any issues with the data. 

The script outputs two csv (.txt) files that contain the counts for all unique dates and then the month aggregated counts.

**plotDates.py**: 

- The python script to create the resulting plot.
- It took some finagling to get matplotlib to play nicely with dates.

**Output/**
- *datesAllkeys.txt*: The counts for all unique dates
- *datesMonthKeys.txt*: The counts by month
- *fig.png*: The plot 

![](1_part/output/fig.png)


## Part 2

**refCounts.py**: This is the main script to run spark commands for part 2.

This problem felt pretty straightforward. I used the same process as in part 1 to return all of the counts for the unique DOI titles.

**parseDoi.py**: 

- This script automates the parseing of DOI strings to return summary metrics (author, year, title, etc). 
- It runs a curl bash command in python and parses the output.
- Ex. `'curl -LH "Accept: text/bibliography; style=bibtex" http://dx.doi.org/'`
- It writes a modified output of the raw DOI counts.
- It is a little clumsy but it is automated.

**Output/**

- *refCounts.txt*: The file containing raw DOI and counts
- *refDOItitles.txt*: File after updating the DOI with information


## Part 3

**pubCounts.py**: This is the main script to run spark commands for part 3.

This problem included some interesting aspects of joining the two RDDs and helped to better understand what the RDD objects where and what exactly broadcasting and parallelizing do. What I ended up doing did not use either of these, but I read in each as an RDD.textFile, got each RDD into a value key format and then used the `RDD.join` command to bring them together. From here I was then just able to sort and write the output the same as before.

**Output/**
- *pubCounts.txt*: Contains the DOI, count, and publisher


## Part 4


### SubPart 1

**sub_01_countryTop20.py**: This is the file that runs the spark commands for the first part of 4.

This script is fairly straightforward and returns the unique countires and the counts for downloads for each. It again usees the `reduceByKey` to get counts, sorts in reverse order and writes the top 20 values.

### Subpart 2

**sub_02_US_cityCounts.py.py**: This is the file that runs the spark commands for the second part of 4.

This problem I am simply filtering to retain only objects that have `country == United States`. Then we need to add the lattitude and longitude to the city to ensure that we do not mix up cities with the same name. However... when I tried this I got Java errors relating to ascii/unicode issues and was unable to resolve these in time while submitting the assignment. Therefore, my script will be grouping cities that share the same name. I tried `str.encode('UTF-8')`, filtering to ensure it was unicode, but still received the same issues. The instructions weren't explicit about which "top cities" to return so I just returned the top 20 cities.

### Subpart 3

**sub_03_portlandCounts.py.py**: This is the file that runs the spark commands for the third part of 4.

For this script I chose to filter by to retain only rows that have `country == United States and city == Portland`. I then split the data into two different objects for Portland Maine and Portland Oregon by filtering checking to see if the lattitude and longitude matched that of Portland Oregon. There may have been better ways to go about this, but this felt like a really simply straightforward way to go about this.

**parsePop.py**

- This script uses pandas and to scrape the wikitable from the population data from late 2015 on wikipedia and adjust the 


**Output/**
- *countryCounts.txt*: Sorted country counts without population adjustment
- *countryCounts_ADJ.txt*: Sorted country counts WITH population adjustment
- *US_cityCounts*: Sorted US city download counts
- *portlandOregon.txt*: Top ten articles for Portland, Oregon
- *portlandMaines.txt*: Top ten articles for Portland, Maine







