
import pandas as pd
import re
import os 

'''
Sources of help for these parts 


- Scraping a wikitable using pandas 
    - https://stackoverflow.com/questions/54890708/scraping-data-from-wikipedia-table
    
'''


##############################################################################
# Parse country pops
##############################################################################

def returnPopulationsCountries():
    
    # Populations in late 2015
    URL = 'https://en.wikipedia.org/wiki/List_of_countries_by_population_in_2015'
    
    # Grab the table using pandas (so easy)
    df = pd.read_html(URL)[1]
    
    # Dict to store keys (countries) and pop
    d = {}
    
    # Iterate through all rows in df
    for ii in range(len(df)):
        
        # Add new countries and their pop (remove all nonalpha)
        d[re.sub(r'[^a-zA-Z]', '', df['Country / territory'][ii])] = df['Population2015(UN estimate)'][ii]

    return d

    
##############################################################################
# Add to my counts
##############################################################################
    
def returnAdjusted(d, fileIn, write = True):
    
    # Read df
    df = pd.read_csv(fileIn, header = None) 
    
    # Store values
    adjusted = []
    
    for ii in range(len(df)):
        
        # Get location and count
        countryItm = re.sub(r'[^a-zA-Z]', '', df[0][ii])
        countItm = float(df[1][ii])
        
        adjusted.append(round(countItm / float(d[countryItm]), 6))
        
    # Add column to df
    df['adjusted'] = adjusted
    
    df.columns = ['Location', 'Downloads', 'PopulationAdjusted']
    
    # Write if specified
    if write:
        
        # Get output name (just add ADJ)
        basePath = os.path.dirname(fileIn)
        outName = os.path.basename(fileIn).split('.')[0] + '_ADJ.txt'
        
        df.to_csv(basePath + '/' + outName, index = False)
    
    return df


##############################################################################
# Run
##############################################################################

if __name__ == "__main__":
    
    # Parent directory
    parDir = '/Users/harmang/Desktop/git_home/coursework/CS_clusters/hw2_spark/4_part/output/'

    # Get population table for countries
    popsCountries = returnPopulationsCountries()
        
    # Return and write dataframe pop adjusted values
    returnAdjusted(popsCountries, parDir + 'countryCounts.txt', False)
    