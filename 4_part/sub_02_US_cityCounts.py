#!/usr/bin/python

from pyspark import SparkContext, SparkConf

################################################################
# Setup pyspark
################################################################

conf = SparkConf().setAppName("app")
sc = SparkContext(conf=conf)


################################################################
# Parse raw scihub data
################################################################

# Load corpus and get unique DOI prefixes and counts
scihubData = sc.textFile('/data/scihub/')
scihubLines = scihubData.filter(lambda x: len(x.split('\t')) == 6)

# Map into (key, value) pairs 
countriesAll = scihubLines.map(lambda x: [x.split('\t')[3].encode('UTF-8'),
										 x.split('\t')[4].encode('UTF-8')])

# Filter some bad possibilities
countriesAllFilt = countriesAll.filter(lambda x: x[0] == 'United States')
countriesUS = countriesAllFilt.map(lambda x: x[1])
countriesUS = countriesUS.filter(lambda x: x not in ['NA', 'N/A', ''])

# Map to key value pair
countriesKeys = countriesUS.map(lambda x: (x, 1))

# Verify correct length and value == 1
countriesKeysFilt = countriesKeys.filter(lambda x: len(x) == 2)
countriesKeysFilt = countriesKeysFilt.filter(lambda x: x[1] == 1)

# Reduce by key into sums of counts
citiesCount = countriesKeysFilt.reduceByKey(lambda x, n: x + n)

# Sort the country counts
citiesCountSort = citiesCount.sortBy(lambda x: x[1], ascending = False)

# Get the top 20
citiesCollect = citiesCountSort.take(20)

# Write output for the first 20
with open('output/US_cityCounts.txt', 'w') as f:
	for ii in range(len(citiesCollect)):
		keyItm = citiesCollect[ii][0]
		valItm = citiesCollect[ii][1]
		print('{} {}'.format(keyItm, valItm))
		f.write(str(keyItm) + ',' + str(valItm) + '\n')


