#!/usr/bin/python

from pyspark import SparkContext, SparkConf

################################################################
# Setup pyspark
################################################################

conf = SparkConf().setAppName("app")
sc = SparkContext(conf=conf)


################################################################
# Parse raw scihub data
################################################################

# Load corpus and get unique DOI prefixes and counts
scihubData = sc.textFile('/data/scihub/')
scihubLines = scihubData.filter(lambda x: len(x.split('\t')) == 6)

# Map into (key, value) pairs 
portlandsAll = scihubLines.map(lambda x: [x.split('\t')[1].encode('UTF-8'),
										 x.split('\t')[3].encode('UTF-8'),
										 x.split('\t')[4],
										 x.split('\t')[-2],
										 x.split('\t')[-1]])

# Filter some bad possibilities
portlandsAllFilt = portlandsAll.filter(lambda x: x[1] == 'United States' and x[2] == 'Portland')

# Filter to get objects of each city separately
portlandMaines = portlandsAllFilt.filter(lambda x: x[-1] != '45.5230622,-122.6764816')
portlandOregon = portlandsAllFilt.filter(lambda x: x[-1] == '45.5230622,-122.6764816')

# Create key pairs for each of these
portlandMainesKeys = portlandMaines.map(lambda x: (x[0], 1))
portlandOregonKeys = portlandOregon.map(lambda x: (x[0], 1))

# Reduce by key into sums of counts
portlandMainesCounts = portlandMainesKeys.reduceByKey(lambda x, n: x + n)
portlandOregonCounts = portlandOregonKeys.reduceByKey(lambda x, n: x + n)

# Sort the counts
portlandMainesCountsSort = portlandMainesCounts.sortBy(lambda x: x[1], ascending = False)
portlandOregonCountsSort = portlandOregonCounts.sortBy(lambda x: x[1], ascending = False)

# Get the top 10 for each 
portlandMainesCollect = portlandCountsSort.take(10)
portlandOregonCollect = portlandCountsSort.take(10)


# Function to write the output
def writeOut(outFile, objc):

	with open(outFile, 'w') as f:
		for ii in range(len(objc)):
			keyItm = portlandCollect[ii][0]
			valItm = portlandCollect[ii][1]
			print('{} {}'.format(keyItm, valItm))
			f.write(str(keyItm) + ',' + str(valItm) + '\n')


# Write the output
writeOut('output/portlandMaine.txt', portlandMainesCollect)
writeOut('output/portlandMaine.txt', portlandOregonCollect)
