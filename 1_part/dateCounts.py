#!/usr/bin/python

from pyspark import SparkContext, SparkConf
import datetime


# Setup pyspark
conf = SparkConf().setAppName("app")
sc = SparkContext(conf=conf)

# Load corpus
data = sc.textFile('/data/scihub/')

# Filter to get those with only six lines
data = data.filter(lambda x: len(x.split('\t')) == 6)

# Split into lines
lines = data.map(lambda x: x.split('\t'))

# Get only date
datesAll = lines.map(lambda x: x[0].split()[0])
datesMonth = datesAll.map(lambda x: x[:7])

# Map all dates into key pairs
datesAllKeys = datesAll.map(lambda x: (x, 1))
datesMonthKeys = datesMonth.map(lambda x: (x, 1))

# Get date counts
datesAllCounts = datesAllKeys.reduceByKey(lambda x, n: x + n)
datesMonthCounts = datesMonthKeys.reduceByKey(lambda x, n: x + n)

datesAllCollect = datesAllCounts.collect()
datesMonthCollect = datesMonthCounts.collect()

# Write output all
with open('output/datesAllKeys.txt', 'w') as f:
	for ii in range(len(datesAllCollect)):
		f.write(datesAllCollect[ii][0] + ',' + str(datesAllCollect[ii][1]) + '\n')


# Write output all
with open('output/datesMonthKeys.txt', 'w') as f:
	for ii in range(len(datesMonthCollect)):
		f.write(datesMonthCollect[ii][0] + ',' + str(datesMonthCollect[ii][1]) + '\n')



