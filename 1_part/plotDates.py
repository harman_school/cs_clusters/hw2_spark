
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas as pd
import scipy



'''
Sources of help for these parts 


- Plotting dates using matplotlib
    - https://stackoverflow.com/questions/9627686/plotting-dates-on-the-x-axis-with-pythons-matplotlib

- Adding row to pandas dataframe
    - https://stackoverflow.com/questions/24284342/insert-a-row-to-pandas-dataframe

'''


# Parent directory
parDir = '/Users/harmang/Desktop/git_home/coursework/CS_clusters/hw2_spark/'


# Read dataframe 
df = pd.read_csv(parDir + '1_part/output/datesAllKeys.txt', header = None)

df.columns = ['date', 'count'] # Rename columsn 


# Convert to datetime
df['date'] = pd.to_datetime(df.date) # Convert to datetime

# Manually add missing start date
df.loc[-1] = [pd.datetime.strptime('2015-11-05', '%Y-%m-%d'), 0]  # adding a row
df.index = df.index + 1  # shifting index
df = df.sort_index()  # sorting by index

# Manually add missing stop date
df.loc[-1] = [pd.datetime.strptime('2015-11-21', '%Y-%m-%d'), 0]  # adding a row
df.index = df.index + 1  # shifting index
df = df.sort_index()  # sorting by index

df = df.sort_values(by='date') # Sort by date

# Plot figure
fig = plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%Y'))
plt.gca().xaxis.set_major_locator(mdates.DayLocator(interval = 15))

# Vect to hold interp for fill
d = scipy.zeros(df.shape[0])

# Plot the line
plt.plot(df['date'], df['count'], color = '#823b2b', alpha = 1,linewidth=1)

# Fill area under the plot
plt.fill_between(df['date'],df['count'], 
                where = df['count'] >= d, 
                color='#cc6d58', alpha = .4)

# Autoformat date
plt.gcf().autofmt_xdate()

# Set labels
plt.ylabel('Number of Downloads', size = 10)
plt.title('Scihub Acitivty Over 6-Months')
plt.tick_params(axis='both', which='major', labelsize=6)
plt.show()