#!/usr/bin/python

from pyspark import SparkContext, SparkConf

# Setup pyspark
conf = SparkConf().setAppName("app")
sc = SparkContext(conf=conf)

# Load corpus
data = sc.textFile('/data/scihub/')

# Filter to get those with only six lines
data = data.filter(lambda x: len(x.split('\t')) == 6)

# Split into lines
lines = data.map(lambda x: x.split('\t'))

# Get only doi
doi = lines.map(lambda x: (x[1], 1))

# Reduce into counts
doiCounts = doi.reduceByKey(lambda x, n: x + n)

# Sort by count
doiCombSort = doiCounts.sortBy(lambda x: x[1], ascending = False)

# Collect to write
collectDoi = doiCombSort.collect()

with open('output/refCounts.txt', 'w') as f:
	for ii in range(len(collectDoi)):
		keyItm = collectDoi[ii][0]
		valItm = collectDoi[ii][1]
		f.write(keyItm.encode('UTF-8') + ',' + str(valItm) + '\n')



