import os
import pandas as pd


'''
Sources of help for these parts 


- Catpure bash cmd output to get DOI info 
    - https://stackoverflow.com/questions/how-to-capture-output-of-curl-from-python-script/32119300
    
'''

##############################################################################
# Command to get curl cmd for piping
##############################################################################

def parseDOI(doi):
    
    # Curl command to run
    out = ('curl -LH "Accept: text/bibliography; style=bibtex" http://dx.doi.org/' + doi)
    
    # Run the actual command
    result = os.popen(out).read()
    
    # Split elements
    val = result.split(',')
    
    # Get element with title
    titleTile = [x for x in val if 'title=' in x]
    authTile = [x for x in val if 'author=' in x]
    yearTile = [x for x in val if 'year=' in x]
    journTile = [x for x in val if 'journal=' in x]
    pagesTile = [x for x in val if 'pages=' in x]
    
    vect = [] # Store values
    
    # Check value exists and then clean    
    vect.append(authTile[0].split('{')[-1]) if len(authTile) == 1 else vect.append('')
    vect.append((yearTile[0].split('{')[-1]).split('}')[0]) if len(yearTile) == 1 else vect.append('')
    vect.append((titleTile[0].split('{')[-1]).split('}')[0]) if len(titleTile) == 1 else vect.append('')
    vect.append((journTile[0].split('{')[-1]).split('}')[0]) if len(journTile) == 1 else vect.append('')
    vect.append((pagesTile[0].split('{')[-1]).split('}')[0]) if len(pagesTile) == 1 else vect.append('')

    # Save output
    outString = '' 

    # Format if they exist
    if vect[0] != '': outString += vect[0] + ', et al. '
    if vect[1] != '': outString +='(' + vect[1] + ') '
    if vect[2] != '': outString += vect[2] + '. '
    if vect[3] != '': outString += vect[3] + ' p:'
    if vect[4] != '': outString += vect[4]

    
    return outString


#Jones, et al. (2013) Strawberry Rhubarb With Extra Bosons. Journal of Quantum Baking 43(3):132-148
##############################################################################
# Command to get curl cmd for piping
##############################################################################
        
if __name__ == "__main__":
    
    # Read doiCounts
    df = pd.read_csv('output/refCounts.txt', header = None)

    # Get names
    doiNames = []
    
    # Add titles
    for ii in df.iloc[:, 0]:
        doiNames.append(parseDOI(ii))
        
    # Add titles
    df['titles'] = doiNames
    
    # Rename and resort column 
    df.columns = ['DOI', 'Count', 'Title'] 
    df = df[['Count', 'DOI', 'Title']]
    
    # Write output
    df.to_csv('output/refDOItitles.csv', index = False)
