#!/usr/bin/python

from pyspark import SparkContext, SparkConf
import numpy as np


################################################################
# Setup pyspark
################################################################

conf = SparkConf().setAppName("app")
sc = SparkContext(conf=conf)


################################################################
# Parse raw scihub data
################################################################

# Load corpus and get unique DOI prefixes and counts
scihubData = sc.textFile('/data/scihub/')
scihubLines = scihubData.filter(lambda x: len(x.split('\t')) == 6)

# Filter DOI values
scihubLines = scihubData.filter(lambda x: len((x.split('\t')[1]).split('/')[0]) == 7)

# Map into (key, value) pairs
scihubDoi = scihubData.map(lambda x: ((x.split('\t')[1]).split('/')[0], 1))
scihubDoi = scihubDoi.filter(lambda x: len(x[0]) == 7)

# Reduce by key into sums of counts
scihubRed = scihubDoi.reduceByKey(lambda x, n: x + n)


################################################################
# Parse publisher data
################################################################

# Get publisher info
doiInfoData = sc.textFile("file:///l2/corpora/scihub/publisher_DOI_prefixes.csv")

# Get and remove header
header = doiInfoData.first()
doiInfoData = doiInfoData.filter(lambda x: x != header)
doiInfo = doiInfoData.map(lambda x: (x.split(',')[2], x.split(',')[1]))


################################################################
# Combine them sort and write
################################################################

# Combine the two RDDs
doiComb = scihubRed.join(doiInfo)

# Sort by ref
doiCombSort = doiComb.sortBy(lambda x: x[1], ascending = False)

# Collect to write
collectDoi = doiCombSort.collect()

# Write output
with open('output/pubCounts.txt', 'w') as f:
	for ii in range(len(collectDoi)):
		keyItm = collectDoi[ii][0]
		valItm = collectDoi[ii][1]
		f.write(str(keyItm) + ',' + str(valItm[0]) + ',' + str(valItm[1]) + '\n')






